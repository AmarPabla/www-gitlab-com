---
extras:
  - title: "Contributions to Git 2.45"
    description: |
      Our team members made significant contributions to [Git 2.45.0](https://gitlab.com/git-vcs/git/-/blob/master/Documentation/RelNotes/2.45.0.txt?ref_type=heads). Here are a
      few.

      ### Reftables reference backend

      Up to now, Git kept track of branches and tags (which both fall into the
      category of "references" or "refs") in loose files in the `.git` directory. Git can also
      pack these up into a single "packed-refs" file. This can lead to:

      - Performance bottlenecks for those running monorepositories that have
        many, many references.
      - Race conditions when references are updated or
        deleted concurrently, leading to secondaries getting out of sync.

      [Reftables](https://www.git-scm.com/docs/reftable) is a next generation
      backend format for references that stores them not in loose files, but in
      an indexed, binary file format. This format both scales with a large
      number of references, and allows atomic updates.

      The format was originally designed by Shawn Pearce in July 2017, and in
      2021 a `reftable` library was upstreamed by Han-Wen Neinhuys, but was not
      integrated into the Git project. The Gitaly team got a significant number
      of changes merged including test refactoring, tooling, prepping the code
      base to integrate the `reftable` backend, and many code optimizations to
      the original implementation.

      The result is that with Git 2.45.0, `reftables` is now usable! This
      enables GitLab to run our Git servers with reftables as the references
      backend for repositories, leading to improved performance for large
      monorepositories.
  
      ### Better tooling for references
      
      With the introduction of the `reftable` backend, tooling was also needed
      to inspect the references given it stores refs in a binary format.

      #### Better tooling for references
      
      Because the `reftable` backend stores references in a binary format rather than loose
      files, we improved Git's tooling for inspecting refs to allow users to
      inspect refs when using `reftable`.

      A new flag was added to `git-for-each-ref(1)` called `--include-root-refs`,
      which will cause it to also list all references that exist in the root of
      the reference naming hierarchy. For example:
  
      ```sh
      $ git for-each-ref --include-root-refs
      f32633d4d7da32ccc3827e90ecdc10570927c77d commit    HEAD
      f32633d4d7da32ccc3827e90ecdc10570927c77d commit    MERGE_HEAD
      f32633d4d7da32ccc3827e90ecdc10570927c77d commit    refs/heads/main
      ```

      #### Listing all reflogs
      
      The [reflog](https://git-scm.com/docs/git-reflog) in Git tracks any
      modifications to references, useful for debugging what changed in a given
      reference. Reflogs are kept in loose files under the `.git/logs` directory,
      which can be inspected manually to list all log entries.

      With the `reftable` format, these logs are no longer stored in the
      `.git/logs` directory, making it impossible to inspect through the
      filesystem.

      Git lacked a command to list all reflogs in the directory.  To address
      this problem, a new command (`git-reflog-list(1)`) was added to list all reflogs,
      which works with the `reftable` format.

      #### More efficient packing of references

      When the `reftable` backend is used to add or modify a ref, Git will
      perform "auto-compaction", which merges tables together as needed.

      A `--auto` flag was added to `git-pack-refs(1)` which allows it to skip
      `reftable` compaction when Git detects that the reference database is
      already in an optimal state.
  
      `git-maintenance(1)` has also been adapted to pass this flag when it calls
      `git-pack-refs(1)` to make use of this new mode by default.
