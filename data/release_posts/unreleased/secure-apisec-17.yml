---
features:
  secondary:
  - name: "API Security Testing analyzer updates"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/api_security_testing/'
    reporter: smeadzinger
    stage: secure
    categories:
    - API Security
    issue_url: 'https://gitlab.com/groups/gitlab-org/-/epics/13644'
    description: |
      We published the following API Security Testing analyzer updates during the 17.0 release milestone:
      - System environment variables are now passed from the CI runner to the custom Python scripts used for certain advanced scenarios (like request signing). This will make implementing these scenarios easier. [See the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/457795) for more details.
      - API Security containers now run as a non-root user, which improves flexibility and compliance. [See the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/287702) for more details.
      - Support for servers that only offer TLSv1.3 ciphers, which enables more customers to adopt API Security Testing. [See the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/441470) for more details.
      - Upgrade to Alpine 3.19, which addresses security vulnerabilities. [See the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/456572) for more details. 

      As [previously announced](https://docs.gitlab.com/ee/update/deprecations.html#secure-analyzers-major-version-update), [we increased the major version number of API Security Testing to version 5](https://gitlab.com/gitlab-org/gitlab/-/issues/456874) in GitLab 17.0.
