---
features:
  secondary:
    - name: "Group comment templates"
      available_in: [core, premium, ultimate] # Include all supported tiers
      gitlab_com: true
      documentation_link: 'https://docs.gitlab.com/ee/user/profile/comment_templates.html'
      image_url: '/images/16_11/create-group-comment-templates.png'
      reporter: phikai
      stage: create # Prefix this file name with stage-informative-title.yml
      categories:
        - 'Code Review Workflow'
        - 'Team Planning'
      issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/440817'
      description: |
        Across an organization it can be helpful to have the same templated response in issues, epics, or merge requests. These responses might include standard questions that need to be answered, responses to common problems, or maybe structure for merge request review comments.
        
        Group comment templates enable you to create saved responses that you can apply in comment boxes around GitLab to speed up your workflow. This new addition to comment templates allows organizations to create and manage templates centrally, so all of their users benefit from the same templates.

        To create a comment template, go to any comment box on GitLab and select **Insert comment template > Manage group comment templates**. After you create a comment template, it's available for all group members. Select the **Insert comment template** icon while making a comment, and your saved response will be applied.

        We're really excited about this next iteration of comment templates and will also be adding [project-level comment templates](https://gitlab.com/gitlab-org/gitlab/-/issues/440818) soon too. If you have any feedback, please leave it in [issue 45120](https://gitlab.com/gitlab-org/gitlab/-/issues/451520).
