---
features:
  secondary:
  - name: "Standards Adherence Report Improvements"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/compliance/compliance_center/#standards-adherence-dashboard'
    image_url: '/images/16_9/standards-adherence-grouping.png'
    reporter: g.hickman
    stage: govern
    categories:
    - 'Compliance Management'
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/11053'
    description: |
      The [standards adherence report](https://docs.gitlab.com/ee/user/compliance/compliance_center/#view-the-standards-adherence-dashboard), within the
      [compliance center](https://docs.gitlab.com/ee/user/compliance/compliance_center/), is the destination for compliance teams to monitor their compliance posture.

      In GitLab 16.5, we introduced the report with the GitLab Standard - a set of common compliance requirements all compliance teams should monitor. The standard helps
      you understand which projects meet these requirements, which ones fall short, and how to bring them into compliance. Over time, we'll be introducing more standards
      into the reporting.

      In this milestone, we've made some improvements which will make reporting more robust and actionable. These include:

      - Grouping results by check
      - Filtering by project, check, and standard
      - Export to CSV (delivered via email)
      - Improved pagination
