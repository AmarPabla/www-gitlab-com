---
features:
  secondary:
  - name: "More detailed security findings in VS Code"
    available_in: [ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/editor_extensions/visual_studio_code/'
    image_url: '/images/16_9/vs-code-security-finding-details.png'
    reporter: connorgilbert
    stage: secure
    categories:
    - Editor Extensions
    - API Security
    - Container Scanning
    - DAST
    - Fuzz Testing
    - SAST
    - Secret Detection
    - Software Composition Analysis
    - Vulnerability Management
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/10996'
    description: |
      We've improved how security findings are shown in the [GitLab Workflow extension](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow#security-findings) for Visual Studio Code (VS Code).
      You can now see more details of your security findings that weren't previously shown, including:

      - Full descriptions, with rich-text formatting.
      - The solution to the vulnerability, if one is available.
      - A link to the location where the problem occurs in your codebase.
      - Links to more information about the type of vulnerability discovered.

      We've also:

      - Improved how the extension shows the status of security scans before results are ready.
      - Made other usability improvements.
