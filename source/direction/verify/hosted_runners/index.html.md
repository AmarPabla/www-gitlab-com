---
layout: markdown_page
title: "Category Direction - Hosted runners"
description: "This is the Product Direction Page for the Hosted runners product category."
---

## Hosted runners

| -                     | -                              |
| Stage                 | [Verify](/direction/verify/)   |
| Maturity              | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2024-04-18`                   |

### Introduction

Thanks for visiting this direction page on the Hosted runners category at GitLab. This page belongs to the [Hosted runners Group](https://about.gitlab.com/handbook/product/categories/#runner-saas-group) within the Verify Stage and is maintained by [Gabriel Engel](mailto:gengel@gitlab.com). Feel free to follow our [YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqcTxGhFdJtcUpvjdf05MHY) where we will upload walkthroughs of direction page updates and iteration kickoffs.

### Strategy and Themes

With the adoption of a DevSecOps approach and the resulting gains in developer and delivery efficiency, organizations are seeing an increased demand for computing power to run CI/CD pipelines.

Our **vision** is to provide a fully managed, best-in-class hosted CI/CD build infrastructure that is fast and secure by default. We want to eliminate the overhead of hosting and maintaining your build infrastructure, ultimately enabling you to deliver software faster. Our platform approach will allow you to run cross-platform (Docker containers on Linux, Windows, macOS) builds in one CI/CD pipeline. With GitLab-hosted runners, we aim to help you realize increased cost savings and efficiency by providing a reliable service for running all CI/CD builds that do not need to run on your infrastructure.

Today, the [GitLab-hosted runners](https://docs.gitlab.com/ee/ci/runners/) product offerings are:

- [Hosted runners on Linux - GA](https://docs.gitlab.com/ee/ci/runners/saas/linux_saas_runner.html)
- [GPU-enabled hosted runners - GA](https://docs.gitlab.com/ee/ci/runners/saas/gpu_saas_runner.html)
- [Hosted runners on macOS - Beta](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html)
- [Hosted runners on Windows - Beta](https://docs.gitlab.com/ee/ci/runners/saas/windows_saas_runner.html)

### 1 year plan

The primary FY2025 runner themes focus on building a competitive and complete CI/CD offering that allows customers to build for all common platforms.

- [CI Steps (GA)](https://gitlab.com/groups/gitlab-org/-/epics/11525) to support both [CI Components Catalog](https://about.gitlab.com/direction/verify/component_catalog) and CI Events
- [GRIT - Linux Docker template on AWS (GA)](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/2) to enable hosted runners on GitLab Dedicated
- [Hosted runners on macOS](https://gitlab.com/groups/gitlab-org/-/epics/8267) transition to General Availability
- [Add ARM compute to hosted runners on Linux](https://gitlab.com/groups/gitlab-org/-/epics/8442)
- [Large hosted runners on macOS](https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/107)
- [Hosted runners on Windows](https://gitlab.com/groups/gitlab-org/-/epics/2162) transition to General Availability

In addition we want to reduce operational overheads by dogfooding our new autoscaler & GRIT for our fleet of runners.
- [Transition Hosted runners on Linux to new Autoscaler](https://gitlab.com/groups/gitlab-org/ci-cd/shared-runners/-/epics/17)
- [Establish GitLab Runner Infrastructure Toolkit (GRIT) for GCP & AWS](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/1) and transition our fleet to GRIT

### 3 year plan

In the next three years, we want to enable enterprises, to fully focus on developing software without the hassle of managing infrastructure and make GitLab-hosted runners, to be the most widely adopted CI solution by enterprises in the market. We want to achieve this, by focusing on [speed](https://about.gitlab.com/company/team/structure/working-groups/ci-build-speed/), security, and cost efficiency.

Our flagship offering will be [custom-hosted runners](https://gitlab.com/groups/gitlab-org/-/epics/10073), allowing users to create [custom GitLab-hosted runners via GitLab UI](https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/148), fully dedicated to their project, with more advanced features such as [dedicated IP-range](https://gitlab.com/gitlab-org/gitlab/-/issues/364425), or [SSH access for debugging](https://gitlab.com/groups/gitlab-org/-/epics/10423). Combined with a new pricing model, it should be a superior choice for the enterprise hosting needs.

### Current scope

This section will give a high-level overview of what we are currently working on. Feel free to check our [FY25 roadmap planning issue](https://gitlab.com/groups/gitlab-org/ci-cd/shared-runners/-/epics/18) or our specific [iteration planning issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=title_asc&state=opened&label_name%5B%5D=Category%3ARunner%20SaaS&label_name%5B%5D=Planning%20Issue&first_page_size=20) for more details.

#### What we recently completed
<!-- Lookback limited to 3 months. Link to the relevant issues or release post items. -->

- [CI Steps (Experiment)](https://gitlab.com/groups/gitlab-org/-/epics/11736) with a [video demo](https://www.youtube.com/watch?v=TU_53vWVKeE) showcasing how to use CI Steps
- [GRIT - Linux Docker template on AWS (Experiment)](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/4) to enable hosted runners on GitLab Dedicated
- [GRIT - Linux Docker template on Google Cloud (Experiment)](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/7) to enable the [runner on Google Cloud integration](https://docs.gitlab.com/ee/ci/runners/provision_runners_google_cloud.html)
- [Dogfooding new autoscaler with GCP plugin on internal linux runner fleet](https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/87)

#### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

- [Viable feature maturity for CI Steps](https://gitlab.com/groups/gitlab-org/-/epics/11525)
- [Add ARM compute to hosted runners on Linux](https://gitlab.com/groups/gitlab-org/-/epics/8442)
- [Large hosted runners on macOS](https://gitlab.com/gitlab-org/ci-cd/shared-runners/infrastructure/-/issues/107)

#### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to epics encompass a vision that is a longer horizon and don't lay out an iteration plan. -->

In the next three months we plan to complete the following projects:

- [Hosted runners on macOS](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html) to [General Availability](https://gitlab.com/groups/gitlab-org/-/epics/8267)
- [Transition GitLab Hosted runners on Linux to new Autoscaler and GRIT](https://gitlab.com/groups/gitlab-org/ci-cd/shared-runners/-/epics/17)
- [GRIT - Linux Docker template on AWS (Beta)](https://gitlab.com/groups/gitlab-org/ci-cd/runner-tools/-/epics/2)
- [GRIT - Linux Docker template on GCP (Beta)](https://gitlab.com/gitlab-org/ci-cd/runner-tools/grit/-/issues/7)

### Best in Class Landscape
<!-- Blanket description consistent across all pages that clarifies what GitLab means when we say "best in class" -->

BIC (Best In Class) is an indicator of forecated near-term market performance based on a combination of factors, including analyst views, market news, and feedback from the sales and product teams. It is critical that we understand where GitLab appears in the BIC landscape.

#### Key Capabilities

Cloud-native CI/CD solutions, such as GitLab.com, Harness.io, CircleCI, and GitHub, allow to run CI/CD pipelines on a fully managed runner fleet with no setup required.

In addition to eliminating CI build server maintenance costs, there are other critical considerations for organizations that can migrate 100% of their CI/CD processes to a cloud-native solution. These include security, reliability, performance, compute types, and on-demand scale.

CI build speed or time-to-result, and the related CI build infrastructure cost efficiency are critical competitive vectors. [CircleCI](https://circleci.com/circleci-versus-github-actions/) and [Harness.io](https://www.harness.io/blog/announcing-speed-enhancements-and-hosted-builds-for-harness-ci) are promoting CI-build performance in their go-to-market strategy.
Both [GitHub](https://github.blog/2022-12-08-experiment-the-hidden-costs-of-waiting-on-slow-build-times/) and [Harness.io](https://www.harness.io/blog/fastest-ci-tool) explored the cost impact of CI build performance measured by build times on hosted solutions.

#### Top Competitive Solutions

**Hosted Linux Compute**

| Size     | Machine Specs       | GitLab        | GitHub    | CircleCI      |
| -------- | ------------------- | ------------- | --------- | ------------- |
| small    | 2 vCPUs, 8GB RAM    | Available     | Available | Available     |
| medium   | 4 vCPUs, 16GB RAM   | Available     | Beta      | Available     |
| large    | 8 vCPUs, 32GB RAM   | Available     | Beta      | Available     |
| x-large  | 16 vCPUs, 64GB RAM  | Available     | Beta      | Available     |
| 2x-large | 32 vCPUs, 128GB RAM | Available     | Beta      | Not available |
| 3x-large | 48 vCPUs, 192GB RAM | Not available | Beta      | Not available |
| 4x-large | 64 vCPUs, 256GB RAM | Not available | Beta      | Not available |

**Hosted GPU Compute**

| Size       | Machine Specs       | GitLab        | GitHub        | CircleCI      |
| ---------- | ------------------- | ------------- | ------------- | ------------- |
| lite       | Nvidia Tesla P4     | Not available | Not available | Available     |
| standard   | Nvidia Tesla T4     | Available     | Not available | Available     |
| premium    | Nvidia Tesla V100   | Not available | Not available | Available     |

**macOS - Offer Positioning and Hosted Build Machines**

| | GitLab | GitHub | Xcode Cloud | CircleCI | Bitrise.io |
|-|--------|--------|------------------------|----------|------------|
| Positioning statement | Hosted runners on macOS provide an on-demand macOS build environment fully integrated with GitLab CI/CD. | A GitHub-hosted runner is VM hosted by GitHub with the GitHub actions runner application installed. | A CI/CD service built into Xcode, designed expressly for Apple developers. | Industry-leading speed. No other CI/CD platform takes performance as seriously as we do. | Build better mobile applications, faster. |
| Value proposition | You can take advantage of all the capabilities of the GitLab single DevOps platform and not have to manage or operate a build environment. | When you use a GitHub-hosted runner, machine maintenance and upgrades are taken care of.|Build your apps in the cloud and eliminate dedicated build infrastructure.| The macOS execution environment allows you to test, build, and deploy macOS and iOS apps on CircleCI. | CI for mobile - save time spent on testing, onboarding, and maintenance with automated workflows and triggers |
| Available machines | Apple silicon (M1): medium | Apple silicon (M1): large; x86-64: small, large | n/a | Apple silicon (M1): medium, large; x86-64: medium, x-large | Apple silicon (M1): medium, large; x86-64: medium, large, x-large |
