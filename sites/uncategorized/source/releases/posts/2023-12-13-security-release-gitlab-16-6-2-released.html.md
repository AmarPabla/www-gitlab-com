---
title: "GitLab Security Release: 16.6.2, 16.5.4, 16.4.4"
categories: releases
author: Dominic Couture
author_gitlab: dcouture
author_twitter: gitlab
description: "Learn more about GitLab Security Release: 16.6.2, 16.5.4, 16.4.4 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 16.6.2, 16.5.4, and 16.4.4 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases patches for vulnerabilities in dedicated security releases. There are two types of security releases:
a monthly, scheduled security release, released a week after the feature release (which deploys on the 3rd Thursday of each month),
and ad-hoc security releases for critical vulnerabilities. For more information, you can visit our [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of our regular and security release blog posts [here](/releases/categories/releases/).
In addition, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest security release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Table of fixes

| Title | Severity |
| ----- | -------- |
| [Smartcard authentication allows impersonation of arbitrary user using user's public certificate](#smartcard-authentication-allows-impersonation-of-arbitrary-user-using-user's-public-certificate) | High |
| [When subgroup is allowed to merge or push to protected branches, subgroup members with the Developer role may gain the ability to push or merge](#when-subgroup-is-allowed-to-merge-or-push-to-protected-branches,-subgroup-members-with-the-developer-role-may-gain-the-ability-to-push-or-merge) | Medium |
| [The GitLab web interface does not ensure the integrity of information when downloading the source code from installation packages or tags.](#the-gitlab-web-interface-does-not-ensure-the-integrity-of-information-when-downloading-the-source-code-from-installation-packages-or-tags) | Medium |
| [Project maintainer can escalate to Project owner using project access token rotate API](#project-maintainer-can-escalate-to-project-owner-using-project-access-token-rotate-api) | Medium |
| [Omission of Double Encoding in File Names Facilitates the Creation of Repositories with Malicious Content.](#omission-of-double-encoding-in-file-names-facilitates-the-creation-of-repositories-with-malicious-content) | Medium |
| [Unvalidated timeSpent value leads to unable to load issues on Issue board](#unvalidated-timespent-value-leads-to-unable-to-load-issues-on-issue-board) | Medium |
| [Developer can bypass predefined variables via REST API](#developer-can-bypass-predefined-variables-via-rest-api) | Medium |
| [Auditor users can create merge requests on projects they don't have access to](#auditor-users-can-create-merge-requests-on-projects-they-dont-have-access-to) | Low |

### Smartcard authentication allows impersonation of arbitrary user using user's public certificate

An improper certificate validation issue in Smartcard authentication in GitLab EE affecting all versions from 11.6 prior to 16.4.4, 16.5 prior to 16.5.4, and 16.6 prior to 16.6.2 allows an attacker to authenticate as another user given their public key if they use Smartcard authentication. Smartcard authentication is an experimental feature and has to be manually enabled by an administrator. This is a high severity issue (`CVSS:3.1/AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:N`, 7.4). It is now mitigated in the latest release and is assigned [CVE-2023-6680](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6680).

Thanks Lucas Serrano from PEReN ([@LSerranoPEReN](https://gitlab.com/LSerranoPEReN)) for reporting this vulnerability.

## When subgroup is allowed to merge or push to protected branches, subgroup members with the Developer role may gain the ability to push or merge

An issue has been discovered in GitLab EE Premium and Ultimate affecting versions 16.4.3, 16.5.3, and 16.6.1. In projects using subgroups to define who can push and/or merge to protected branches, there may have been instances in which subgroup members with the Developer role were able to push or merge to protected branches. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:H/A:N`, 6.5). It is now mitigated in the latest release and is assigned [CVE-2023-6564](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6564).

This vulnerability has been discovered internally by a GitLab team member.

The following script can help you identify projects that may be subject to a vulnerable configuration. This script can be used to create a CSV file listing projects that have a group set as "Allowed to merge" or "Allowed to push and merge" along with the web_url and project_id for the project and the group_name/group_id for the group. Note that this is not an indication that unauthorized changes were made to protected branches, but rather an indication that these projects were subject to this vulnerable configuration. For impacted projects, customers will need to check merge requests that were merged on their self-managed GitLab instances running 16.4.3, 16.5.3, or 16.6.1 prior to updating to 16.4.4, 16.5.4, or 16.6.2 or on GitLab.com prior to 2023-12-04 18:10 UTC.

<details><summary>Click to expand the script</summary>

```sh
## install `glab` (if not already installed)
# https://gitlab.com/gitlab-org/cli#installation
## install `jq` (if not already installed)
# https://jqlang.github.io/jq/download/

# authenticate with `glab` as Admin (self-managed) or group owner (SaaS)
glab auth login

## get `project_id` and `web_url` for all projects at the instance level (self-managed) or group level (SaaS), save it as `project-list.csv`
# self-managed - instance level (use Admin PAT for authentication)
glab api --hostname "self-managed-gitlab.example.com" --paginate projects 2>> error.log | jq -c '.[]' | jq -rc '[.id, .web_url] | @csv' | tee -a project-list.csv
# SaaS - group level (use group owner PAT for authentication)
glab api --paginate "groups/$GROUP_ID/projects" 2>> error.log | jq -c '.[]' | jq -rc '[.id, .web_url] | @csv' | tee -a project-list.csv 

## add headers to protected_branch_report.csv file
echo "project_id, web_url, group_name_push_access, group_id_push_access, group_name_merge_access, group_id_merge_access" > protected_branch_report.csv

## loop through each project to check for protected branches that have a group with push or merge access
while IFS=',' read -r PROJECT_ID WEB_URL; do
  glab api "projects/$PROJECT_ID/protected_branches" 2>> error.log \
  | jq -c '.[]' \
  | jq 'select((any(.push_access_levels[]; .group_id != null and .access_level == 40)) or (any(.merge_access_levels[]; .group_id != null and .access_level == 40)))' 2>> error.log \
  | jq -c "{project_id: $PROJECT_ID, web_url: $WEB_URL, group_id_push_access: .push_access_levels.[].group_id, group_name_push_access: .push_access_levels.[].access_level_description, group_id_merge_access: .merge_access_levels.[].group_id, group_name_merge_access: .merge_access_levels.[].access_level_description}" 2>> error.log \
  | jq 'select((.group_id_push_access != null or .group_id_merge_access != null) and (.group_name_push_access != "Maintainers" or .group_name_merge_access != "Maintainers"))' 2>> error.log \
  | jq -rc '[.project_id, .web_url, .group_name_push_access, .group_id_push_access, .group_name_merge_access, .group_id_merge_access] | @csv' \
  | tee -a protected_branch_report.csv  
done < project-list.csv
```

GitLab has conducted limited testing to validate this script. As such this script is provided AS-IS and GitLab makes no warranties of any kind. GITLAB HEREBY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, AND NON-INFRINGEMENT.

</details>

### The GitLab web interface does not ensure the integrity of information when downloading the source code from installation packages or tags.

An issue has been discovered in GitLab CE/EE affecting all versions before 16.4.4, all versions starting from 16.5 before 16.5.4, all versions starting from 16.6 before 16.6.2. File integrity may be compromised when source code or installation packages are pulled from a specific tag.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:H/A:N`, 5.7).
It is now mitigated in the latest release and is assigned [CVE-2023-6051](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-6051).

Thanks [st4nly0n](https://hackerone.com/st4nly0n) for reporting this vulnerability through our HackerOne bug bounty program.


### Project maintainer can escalate to Project owner using project access token rotate API

A privilege escalation vulnerability in GitLab EE affecting all versions from 16.0 prior to 16.4.4, 16.5 prior to 16.5.4, and 16.6 prior to 16.6.2 allows a project Maintainer to use a Project Access Token to escalate their role to Owner.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:N/I:H/A:N`, 4.9).
It is now mitigated in the latest release and is assigned [CVE-2023-3907](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3907).

Thanks [ashish_r_padelkar](https://hackerone.com/ashish_r_padelkar) for reporting this vulnerability through our HackerOne bug bounty program.


### Omission of Double Encoding in File Names Facilitates the Creation of Repositories with Malicious Content.

An issue has been discovered in GitLab CE/EE affecting all versions from 16.3 before 16.4.4, all versions starting from 16.5 before 16.5.4, all versions starting from 16.6 before 16.6.2. File integrity may be compromised when specific HTML encoding is used for file names leading for incorrect representation in the UI.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:U/C:N/I:H/A:N`, 4.8).
It is now mitigated in the latest release and is assigned [CVE-2023-5512](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5512).

Thanks [st4nly0n](https://hackerone.com/st4nly0n) for reporting this vulnerability through our HackerOne bug bounty program.


### Unvalidated timeSpent value leads to unable to load issues on Issue board

An issue has been discovered in GitLab EE affecting all versions starting before 16.4.4, all versions starting from 16.5 before 16.5.4, all versions starting from 16.6 before 16.6.2. It was possible to overflow the time spent on an issue that altered the details shown in the issue boards.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`, 4.3).
It is now mitigated in the latest release and is assigned [CVE-2023-3904](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3904).

Thanks [toukakirishima](https://hackerone.com/toukakirishima) for reporting this vulnerability through our HackerOne bug bounty program.

### Developer can bypass predefined variables via REST API

An issue has been discovered in GitLab affecting all versions starting from 9.3 before 16.4.4, all versions starting from 16.5 before 16.5.4, all versions starting from 16.6 before 16.6.2. In certain situations, it may have been possible for developers to override predefined CI variables via the REST API. This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:L/A:N`, 4.3). It is now mitigated in the latest release and is assigned [CVE-2023-5061](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-5061).

Thanks [ali_shehab](https://hackerone.com/ali_shehab) for reporting this vulnerability through our HackerOne bug bounty program.

### Auditor users can create merge requests on projects they don't have access to

An issue has been discovered in GitLab EE affecting all versions starting from 8.17 before 16.4.4, all versions starting from 16.5 before 16.5.4, all versions starting from 16.6 before 16.6.2. It was possible for auditor users to fork and submit merge requests to private projects they're not a member of. This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:H/UI:R/S:U/C:N/I:L/A:N`, 2.0). It is now mitigated in the latest release and is assigned [CVE-2023-3511](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-3511).

Thanks [js_noob](https://hackerone.com/js_noob) for reporting this vulnerability through our HackerOne bug bounty program.

## Non Security Patches

### 16.6.2

* [[Backport 16.6] Fix redis-namspace dependency version for UBI mailroom](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1649)
* [Fix backup id parsing from backup URLs (16.6 Backport)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1658)
* [Package Registry: Truncate Pypi metadata description field](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138449)
* [Fix adding confidential child tasks](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138535)
* [Backport Hide obsolete migration warning into 16.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138797)
* [[16.6 Backport] Fix Admin Mode bug in DeactivateDormantUsersWorker](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138911)
* [Backport Fix cluster reindexing service preflight check to 16.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138929)
* [Backport Sanitize string provided to to_tsvector](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138804)
* [Backport "Update migration to work for any fk name" to 16.6](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138602)
* [Fix Environment destroy job is retried endlessly (16.6 backport)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/139051)
* [Allow users to authenticate via OAuth with password-based providers](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138431)
* [Do not scan entire /var/opt/gitlab for stale pids (16.6 backport)](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7275)

### 16.5.4

* [gitlab-rails: support skipping post-migrations in db checks](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1641)
* [[Backport 16.5] Fix redis-namspace dependency version for UBI mailroom](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1650)
* [Backport - Truncate verification failure message to 255](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/137712)
* [[16.5 Backport] Fix Admin Mode bug in DeactivateDormantUsersWorker](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138912)

### 16.4.4

* [[Backport 16.4] Fix redis-namspace dependency version for UBI mailroom](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1651)
* [[16.4 Backport] Fix Admin Mode bug in DeactivateDormantUsersWorker](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/138913)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Security Release Notifications

To receive security release blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [security release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).
